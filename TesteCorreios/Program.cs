﻿using FluentAssertions;
using RestSharp;
using System;
using Xunit;

namespace TesteCorreios
{
    public class Program
    {
        private IRestResponse GetCep(object CEP)
        {
            var client = new RestClient("https://viacep.com.br/ws/" + CEP + "/json/");
            var RSrequest = new RestRequest(Method.GET) { RequestFormat = DataFormat.Json };

            return client.Execute(RSrequest);
        }


        [Fact]
        public void Sucesso()
        {
            var reponse = GetCep("01552001");
            reponse.StatusCode.Should().Be(200);
        }
    }
}
